# Omnifood-Website

A front-end web design project of a food ordering website.

## Technologies used

1. HTML5
2. CSS3
3. JavaScript
4. Hamburger menu by Jonathan Suh (https://jonsuh.com/hamburgers/)

## Usage

Download the code and run the `index.html` file
