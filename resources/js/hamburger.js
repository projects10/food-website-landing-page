let hamburger = document.querySelector('.hamburger');
let navbar = document.querySelector('.navbar nav');

hamburger.addEventListener('click', function () {
  navbar.classList.toggle('navbar-visible');
  hamburger.classList.toggle('is-active');
});
